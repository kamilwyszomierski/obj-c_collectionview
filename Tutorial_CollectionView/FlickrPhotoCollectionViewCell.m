//
//  FlickrPhotoCollectionViewCell.m
//  Tutorial_CollectionView
//
//  Created by Filo on 30.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import "FlickrPhotoCollectionViewCell.h"
#import "FlickrPhoto.h"

@implementation FlickrPhotoCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - 

- (void)setPhoto:(FlickrPhoto *)photo
{
    if (_photo != photo) {
        _photo = photo;
    }
    
    self.imageView.image = _photo.thumbnail;
}

@end
