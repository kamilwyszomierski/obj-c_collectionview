//
//  FlickrPhotoHeaderView.h
//  Tutorial_CollectionView
//
//  Created by Filo on 30.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoHeaderView : UICollectionViewCell

- (void)setSearchText:(NSString *)text;

@end
