//
//  FlickrPhotoViewController.h
//  Tutorial_CollectionView
//
//  Created by Filo on 30.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlickrPhoto;

@interface FlickrPhotoViewController : UIViewController

@property (strong, nonatomic) FlickrPhoto *flickrPhoto;

@end
